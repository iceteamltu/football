import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Player } from '../squad';
import { Rank, Standing } from '../rank';
import { Upgame } from '../upmatch';
import { Finished } from '../finished';


const httpOptionsA = {
	headers: new HttpHeaders ({
		'X-Auth-Token': '7692f3f23db64453916786b5d42481ff'
	})
};

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http: HttpClient) { }

  getSquad(): Observable<Player>{
    return this.http.get<Player>('http://api.football-data.org/v2/teams/1769', httpOptionsA)
  }


  getRank(): Observable<Rank>{
    return this.http.get<Rank>('http://api.football-data.org/v2/competitions/2013/standings', httpOptionsA)
  }

  getUpmatch(): Observable<Upgame>{
    return this.http.get<Upgame>('http://api.football-data.org/v2/teams/1769/matches?status=SCHEDULED', httpOptionsA)
  }

  getFinised(): Observable<Finished>{
    return this.http.get<Finished>('http://api.football-data.org/v2/teams/1769/matches?status=FINISHED', httpOptionsA)
  }

}
