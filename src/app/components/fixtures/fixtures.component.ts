import { Component, OnInit } from '@angular/core';
import { Upgame, Match } from 'src/app/upmatch';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-fixtures',
  templateUrl: './fixtures.component.html',
  styleUrls: ['./fixtures.component.css']
})
export class FixturesComponent implements OnInit {
upgame: Upgame;
match : Match[];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.loadUpgame();

  }
  loadUpgame():void{

    this.dataService.getUpmatch().subscribe(data => {
      this.upgame = data;
      this.match=this.upgame.matches;
      console.log(this.upgame);
    });
  }

}
