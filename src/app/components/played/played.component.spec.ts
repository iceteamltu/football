import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayedComponent } from './played.component';

describe('PlayedComponent', () => {
  let component: PlayedComponent;
  let played: ComponentFixture<PlayedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlayedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    played = TestBed.createComponent(PlayedComponent);
    component = played.componentInstance;
    played.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
