import { Component, OnInit } from '@angular/core';
import { Finished, Match } from 'src/app/finished';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-played',
  templateUrl: './played.component.html',
  styleUrls: ['./played.component.css']
})

export class PlayedComponent implements OnInit {
finished: Finished;
match : Match[];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.loadFinished();

  }
  loadFinished():void{

    this.dataService.getFinised().subscribe(data => {
      this.finished = data;
      this.match=this.finished.matches;
      console.log(this.finished);
    });
  }

}