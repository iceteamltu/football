import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FixturesComponent } from './components/fixtures/fixtures.component';
import { PlayersComponent } from './components/players/players.component';
import { RouterModule } from '@angular/router';
import { StandingsComponent } from './components/standings/standings.component';
import { PlayedComponent } from './components/played/played.component';


@NgModule({
  declarations: [
    AppComponent,
    FixturesComponent,
    PlayersComponent,
    StandingsComponent,
    PlayedComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forChild([
      {path: 'components/players', component: PlayersComponent},
      {path: 'components/standings', component: StandingsComponent},
      {path: 'components/fixtures', component: FixturesComponent},
      {path: 'components/played', component: PlayedComponent},
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
